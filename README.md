# Labor Krone COVID 19 Selbstauskunft Telegram Bot

Kleines, unsauberes Skript, welches prüft, ob ein Befund bei [Labor Krone](https://covid19.labor-daten.de/patient/input.html) vorliegen.

## Setup

Siehe [.env.example](.env.example)
