FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /app
RUN pip install poetry
COPY . /app
RUN poetry config virtualenvs.create false
RUN poetry install
CMD ["python3", "/app/labor_krone_watcher/main.py"]
