import os
import time

import requests
from dotenv import load_dotenv

from labor_krone_watcher.proxy import SuperProxy
from labor_krone_watcher.worker import fetch_result

load_dotenv(override=True)

try:
    telegram_bot_token = os.environ["TELEGRAM_BOT_TOKEN"]
    telegram_chat_id = os.environ["TELEGRAM_CHAT_ID"]
    krone_id = os.environ["KRONE_ID"]
    krone_dob = os.environ["KRONE_DOB"]
    proxy = SuperProxy(os.environ["LUMINATI_USERNAME"], os.environ["LUMINATI_PASSWORD"])
    interval = int(os.environ["KRONE_INTERVAL"])
except KeyError:
    print("Please set the according environment variables")
    raise
except ValueError:
    print("Invalid value for interval")
    raise

if __name__ == "__main__":
    while True:
        try:
            result = fetch_result(krone_id, krone_dob, proxy=proxy)
            res = requests.post(
                "https://api.telegram.org/bot%s/sendMessage" % telegram_bot_token,
                json={
                    "chat_id": telegram_chat_id,
                    "text": "BEFUND GEFUNDEN!!"
                    if result
                    else "Es wurde leider kein Befund gefunden.",
                    "silent": not result,
                },
            )
            if not res.ok:
                print("Failed to send telegram message: %s" % res.json())
            print("Result: %s" % result)
            if result is True:
                print("Received a positive result!")
                break
        except Exception as e:
            print("Error while fetching result: %s" % e)
            continue
        time.sleep(900)
