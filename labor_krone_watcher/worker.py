from requests_html import HTMLSession


def fetch_result(order, dob, proxy=None) -> bool:
    session = HTMLSession()
    session.proxies = proxy.proxy
    res = session.post(
        "https://covid19.labor-daten.de/patient/result.html",
        data={"ordernumber": order, "birthdate": dob},
        headers={
            "referer": "https://covid19.labor-daten.de/patient/input.html",
            "origin": "https://covid19.labor-daten.de",
            "host": "covid19.labor-daten.de",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36",
            "content-type": "application/x-www-form-urlencoded",
        },
    )

    res.raise_for_status()

    has_warning = res.html.find('img[src="css/img/Meldung.svg"]')

    return len(has_warning) == 0
