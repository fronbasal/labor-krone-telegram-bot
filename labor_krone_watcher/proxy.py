class SuperProxy:
    """
    This class represents a luminati.io SuperProxy and provides a management interface for requests.py
    """

    def __init__(self, username, password, port=22225):
        self.proxy_url = "http://%s-country-de:%s@zproxy.lum-superproxy.io:%d/" % (
            username,
            password,
            port,
        )

    @property
    def proxy(self):
        """
        returns proxy representation for requests.py
        """
        return {"http": self.proxy_url, "https": self.proxy_url}
